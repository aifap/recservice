import pandas
import pickle
import os
import boto3
import redis
from collections import defaultdict

RATINGS_COLUMNS = ['user_id', 'post_id', 'score', 'created_at']
#VIEWS_COLUMNS = ['user_id', 'post_id', 'created_at']
USERS_COLUMNS = ['user_id', 'gender_filter', 'media_filter', 'artstyle_filter', 'created_at']
POSTS_COLUMNS = ['post_id', 'title', 'type_gender', 'type_artstyle', 'type_media', 'subreddit_id', 'tags', 'created_at']

RATINGS_CSV = './data/ratings.csv'
#VIEWS_CSV = './data/views.csv'
USERS_CSV = './data/users.csv'
POSTS_CSV = './data/posts.csv'

posts_path = os.path.expanduser('./data/posts.csv')
ratings_path = os.path.expanduser('./data/user_ratings.csv')
users_path = os.path.expanduser('./data/users.csv')
#views_path = os.path.expanduser('./data/views.csv')

remote_posts_path = 'posts.part_00000'
remote_ratings_path = 'ratings.part_00000'
remote_users_path = 'users.part_00000'
#remote_views_path = 'views.part_00000'

class Dataset:
    def __init__(self):
        self.ratings = pandas.DataFrame([], columns=RATINGS_COLUMNS)
#        self.views = pandas.DataFrame([], columns=VIEWS_COLUMNS)
        self.users = pandas.DataFrame([], columns=USERS_COLUMNS)
        self.posts = pandas.DataFrame([], columns=POSTS_COLUMNS)
        
        self.content_score_cache = {}
        self.profile_cache = {}
        self.unviewed_cache = {}
        
    def load_data(self, ratings, views, users, posts):
        self.ratings = ratings
#        self.views = views
        self.users = users
        self.posts = posts
        
    def load(self, filename):
        store = pandas.HDFStore(filename)
        self.ratings = store.get('ratings')
#        self.views = store.get('views')
        self.users = store.get('users')
        self.posts = store.get('posts')
        store.close()
        
    def save(self, filename):
        # Delete existing so that we don't append
        if os.path.isfile(filename):
            os.remove(filename)
            
        store = pandas.HDFStore(filename)
        self.ratings.to_hdf(store, 'ratings')
#        self.views.to_hdf(store, 'views')
        self.users.to_hdf(store, 'users')
        self.posts.to_hdf(store, 'posts')
        store.close()
    
    def download_csvs(self):
        session = boto3.Session(
            aws_access_key_id='REPLACEME',
            aws_secret_access_key='REPLACEME',
        )
        s3 = session.resource('s3')
        main_bucket = s3.Bucket(name='REPLACEME')
        
        main_bucket.Object(remote_posts_path).download_file(posts_path)
        main_bucket.Object(remote_ratings_path).download_file(ratings_path)
        main_bucket.Object(remote_users_path).download_file(users_path)
#        main_bucket.Object(remote_views_path).download_file(views_path)
        
    def load_csvs(self):
        ratings = pandas.read_csv('./data/user_ratings.csv', names=RATINGS_COLUMNS, delimiter=',', quotechar='"', escapechar='\\')
        print('Loaded %s ratings' % len(ratings.index))
        
        posts = pandas.read_csv('./data/posts.csv', names=POSTS_COLUMNS, delimiter=',', quotechar='"', escapechar='\\')
        print('Loaded %s posts' % len(posts.index))
        
#        views = pandas.read_csv('./data/views.csv', names=VIEWS_COLUMNS, delimiter=',', quotechar='"', escapechar='\\')
#        print('Loaded %s views' % len(views.index))
#        views.drop_duplicates(subset=['user_id', 'post_id'], keep='last', inplace=True)
#        print('Trimmed to %s views' % len(views.index))
        
        users = pandas.read_csv('./data/users.csv', names=USERS_COLUMNS, delimiter=',', quotechar='"', escapechar='\\')
        print('Loaded %s users' % len(users.index))
        
        posts['tags'] = posts['tags'].apply(lambda v: v.split('|'))
        posts['tags'] = posts['tags'].apply(lambda x: x.remove('N') if 'N' in x else x)
        posts['tags'] = posts['tags'].apply(lambda x: tuple(x) if x else ())
        
        user_ratings = ratings.merge(posts.drop('created_at', 1), 'left', 'post_id')
        
        self.ratings = user_ratings
        self.posts = posts
#        self.views = views
        self.users = users
        
    
    def unviewed_posts(self, external_user_id):
        indexs = None
        if external_user_id in self.unviewed_cache:
            indexs = self.unviewed_cache[external_user_id]
        else :
#            viewed_posts = self.views.set_index('user_id').loc[external_user_id]['post_id']
            r = redis.Redis(host='aifap.net', port=6379, db=0)
            viewed_posts = r.smembers('views:all#' + str(external_user_id))
            viewed_posts = set(int(x) for x in viewed_posts)
        
            indexs = ~self.posts['post_id'].isin(viewed_posts)
            self.unviewed_cache[external_user_id] = indexs
            
        unviewed_posts = self.posts[indexs]
        return unviewed_posts
    
    def generate_profile(self, external_user_id):
        if external_user_id in self.profile_cache:
            return self.profile_cache[external_user_id]
            
        score_matrix = self.ratings.set_index('user_id').loc[external_user_id].groupby(['user_id', 'subreddit_id', 'type_gender', 'type_artstyle', 'type_media', 'tags'])['score'].mean().to_frame().reset_index()
        
        profile = {
            'subreddits': defaultdict(lambda: 0),
            'tags': defaultdict(lambda: 0),
            'types': {
                'artstyle_type': {
                    'real-life': 0,
                    'hentai': 0, 
                    '3d': 0,
                },
                'gender_type': {
                    'gay': 0,
                    'lesbian':  0,
                    'solo-men': 0,
                    'solo-women': 0,
                    'straight': 0,
                    'transexual': 0,
                    'unknown': 0,
                },
                'media_type': {
                    'image': 0,
                    'animated': 0,
                    'video': 0,
                    'text': 0,
                }
            }
        }
        sub_ids = score_matrix['subreddit_id'].unique()
        artstyle_types = score_matrix['type_artstyle'].unique()
        gender_types = score_matrix['type_gender'].unique()
        media_types = score_matrix['type_media'].unique()
        tags = list(score_matrix['tags'].apply(pandas.Series).stack().to_frame().set_index(0).index.unique())
        user_score_matrix = score_matrix.set_index('user_id').loc[external_user_id]
        for sub_id in sub_ids:
            score = user_score_matrix[user_score_matrix['subreddit_id'] == sub_id]['score'].mean()
            profile['subreddits'][sub_id] = score
        for type_artstyle in artstyle_types:
            score = user_score_matrix[user_score_matrix['type_artstyle'] == type_artstyle]['score'].mean()
            profile['types']['artstyle_type'][type_artstyle] = score
        for type_gender in gender_types:
            score = user_score_matrix[user_score_matrix['type_gender'] == type_gender]['score'].mean()
            profile['types']['gender_type'][type_gender] = score
        for type_media in media_types:
            score = user_score_matrix[user_score_matrix['type_media'] == type_media]['score'].mean()
            profile['types']['media_type'][type_media] = score
        for tag in tags:
            score = user_score_matrix[user_score_matrix['tags'].apply(lambda x: tag in x)]['score'].mean()
            profile['tags'][tag] = score
            
        self.profile_cache[external_user_id] = profile
        return profile