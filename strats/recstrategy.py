from abc import ABC, abstractmethod
from collections import namedtuple
 
class RecStrategy(ABC):
    def __init__(self):
        super().__init__()
    
    @abstractmethod
    def train(self, dataset):
        pass

    @abstractmethod
    def recommend(self, external_user_id):
        pass
    
    def makeResult(self, post_id, score, strategy, meta=None):
        return {
            'strategy': strategy,
            'post_id': int(post_id),
            'score': score,
            'meta': meta
        }
        
    def can_recommend_by_id(self):
        return False
        
    def can_recommend_by_profile(self):
        return False